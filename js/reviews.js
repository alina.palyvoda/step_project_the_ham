const previousBtn = document.querySelector(".preview-btn-before");
const nextBtn = document.querySelector(".preview-btn-after");
let photoList = document.querySelector(".authors");
let photos = document.querySelectorAll(".photo-author");
let reviewContent = document.querySelectorAll(".reviews-content");
let activePhoto = document.querySelector(".active-photo");

let index;
let position = 0;
let width = 102;

function getCurrentIndex() {
    photos.forEach((item, i) => {
        if (item.classList.contains("current-photo")) {
            index = i;
        }
    })
}

getCurrentIndex();

function showReviewContent() {
    getCurrentIndex();
    for (let i = 0; i < reviewContent.length; i++) {
        if (i !== index) {
            reviewContent[i].classList.add("hide");
        } else {
            reviewContent[i].classList.remove("hide");
        }
    }
}

showReviewContent()

function insertPhoto() {
    activePhoto.setAttribute("src", "");
    getCurrentIndex();
    let attr = photos[index].getAttribute("src");
    activePhoto.setAttribute("src", attr);
}

insertPhoto();

function right() {
    getCurrentIndex();
    if (index === 0) {
        previousBtn.addEventListener("click", null);
    } else {
        photos[index].classList.remove("current-photo");
        photoList.style.left = position + width + "px";
        position += width;
        index--;
        photos[index].classList.add("current-photo");
        insertPhoto();
        showReviewContent();
    }
}

previousBtn.addEventListener("click", right)

function left() {
    getCurrentIndex();
    if (index === photos.length - 1) {
        nextBtn.addEventListener("click", null);
    } else {
        photos[index].classList.remove("current-photo");
        photoList.style.left = position - width + "px";
        position -= width;
        index++;
        photos[index].classList.add("current-photo");
        insertPhoto();
        showReviewContent();
    }
}

nextBtn.addEventListener("click", left)

photoList.addEventListener("click", (event) => {
    if (event.target.classList.contains("photo-author")) {
        getCurrentIndex();
        let attr = event.target.getAttribute("src")
        for (let i = 0; i < photos.length; i++) {
            if (photos[i].getAttribute("src") === attr) {
                if (i > index) {
                    left()
                } else if (i < index) {
                    right()
                }
            }
        }
    }
})

