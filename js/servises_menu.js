let servicesMenuTabs = document.querySelector("#servicesMenuTabs");
let servicesMenuTabsContent = document.getElementsByClassName("tab-content");
let servicesMenuTab = document.getElementsByClassName("tab-title");
hideTabsContent(1);

servicesMenuTabs.addEventListener("click", event => {
    let target = event.target;
    if (target.className === "tab-title") {
        for (let i = 0; i < servicesMenuTab.length; i++) {
            if (target === servicesMenuTab[i]) {
                showTabsContent(i);
                break;
            }
        }
    }
})

function hideTabsContent(a) {
    for (let i = a; i < servicesMenuTabsContent.length; i++) {
        servicesMenuTabsContent[i].classList.remove('show');
        servicesMenuTabsContent[i].classList.add("hide");
        servicesMenuTab[i].classList.remove('tab-active');
    }
}

function showTabsContent(b) {
    if (servicesMenuTabsContent[b].classList.contains('hide')) {
        hideTabsContent(0);
        servicesMenuTab[b].classList.add('tab-active');
        servicesMenuTabsContent[b].classList.remove('hide');
        servicesMenuTabsContent[b].classList.add('show');
    }
}

















