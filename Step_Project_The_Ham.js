// ------- Header menu ------
let searchIcon = document.querySelector(".search-icon");
searchIcon.addEventListener("click", () => {
    document.querySelector(".search-input").classList.toggle("show")
})

// ------- Services menu ------
let servicesMenuTabs = document.querySelector("#servicesMenuTabs");
let servicesMenuTabsContent = document.getElementsByClassName("tab-content");
let servicesMenuTab = document.getElementsByClassName("tab-title");
hideTabsContent(1);

servicesMenuTabs.addEventListener("click", event => {
    let target = event.target;
    if (target.className === "tab-title") {
        for (let i = 0; i < servicesMenuTab.length; i++) {
            if (target === servicesMenuTab[i]) {
                showTabsContent(i);
                break;
            }
        }
    }
})

function hideTabsContent(a) {
    for (let i = a; i < servicesMenuTabsContent.length; i++) {
        servicesMenuTabsContent[i].classList.remove('show');
        servicesMenuTabsContent[i].classList.add("hide");
        servicesMenuTab[i].classList.remove('tab-active');
    }
}

function showTabsContent(b) {
    if (servicesMenuTabsContent[b].classList.contains('hide')) {
        hideTabsContent(0);
        servicesMenuTab[b].classList.add('tab-active');
        servicesMenuTabsContent[b].classList.remove('hide');
        servicesMenuTabsContent[b].classList.add('show');
    }
}

// ------- Our Amazing Work ------
let workMenuCategory = document.querySelector(".work-menu");
let workMenuItems = document.querySelector(".work-items").children
let maxNumber = 12 // maximum allowed number of images per screen by default

function showWorkMenuItems (a) {
for (let i = a; i < workMenuItems.length; i++) {
        workMenuItems[i].classList.add("invisible")
    }
}
showWorkMenuItems(maxNumber);

workMenuCategory.addEventListener("click", event => {
    let target = event.target;
    let tabs = document.getElementsByClassName("work-menu-tab");
    if (target.classList.contains("work-menu-tab")) {
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].classList.remove("current-tab");
            target.classList.add("current-tab");
        }
        let filter = target.innerText;
        for (let i = 0; i < workMenuItems.length; i++) {
            workMenuItems[i].classList.remove("hide");
            if (workMenuItems[i].dataset.category !== filter &&
                filter !== "All") {
                workMenuItems[i].classList.add("hide");
            }
        }
    }
})

// function createCardBack () {
// let cardBack = document.createElement("div");
// cardBack.innerHTML = `
//     <div class="details">
//         <div class="details-icons d-flex">
//             <a class="icon-1" href="#">
//                 <svg class="icon-1-part-1" width="43" height="42" viewBox="0 0 43 42" fill="none" xmlns="http://www.w3.org/2000/svg">
//                 <rect x="1" y="1" width="41" height="40" rx="20" stroke="#18CFAB"/></svg>
//                 <svg class="icon-1-part-2" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
//                     <path fill-rule="evenodd" clip-rule="evenodd"
//                         d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759143 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922616 9.8266 0.0883684 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
//                         fill="#1FDAB5"/>
//                 </svg>
//             </a>
//             <a class="icon-2" href="#">
//                 <svg class="icon-2-part-1" width="41" height="41" viewBox="0 0 41 41" fill="none" xmlns="http://www.w3.org/2000/svg">
//                     <path fill-rule="evenodd" clip-rule="evenodd"
//                         d="M20.5973 0.997952C31.8653 0.997952 40.9999 9.95227 40.9999 20.9979C40.9999 32.0432 31.8653 40.9979 20.5973 40.9979C9.3292 40.9979 0.194626 32.0432 0.194626 20.9979C0.194626 9.95227 9.3292 0.997952 20.5973 0.997952Z"
//                         fill="#18CFAB"/></svg>
//                 <svg class="icon-2-part-2" width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
//                     <rect width="12" height="11" fill="white"/>
//                 </svg>
//             </a>
//         </div>
//         <span class="details-title">creative design</span>
//         <span class="details-section">Web Design</span>
//     </div>
//     `;
// }
//
// document.querySelector(".work-items").addEventListener("click", event => {
//     let target = event.target;
//     console.log(target)
//     if (target === event.currentTarget) {
//         return
//     }
// })

let loadBtn = document.querySelector(".load-more-btn");
loadBtn.addEventListener("click", () => {
    setTimeout(function () {
        maxNumber = maxNumber * 2;
        for (let i = 0; i < workMenuItems.length; i++) {
            workMenuItems[i].classList.remove("invisible")
        }
        showWorkMenuItems(maxNumber)
    }, 2000)

})












